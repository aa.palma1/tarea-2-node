const Tweets = require("../models/tweets");
//const { errorHandler } = require("../helpers/dbErrorHandler");

// middlewares rest

exports.tweetById = (req, res, next, id) => {
    Tweets.findById(id).exec((err, tweets) => {
        if (err || !tweets) {
            return res.status(404).json({
                error: "Tweets does not exist"
            });
        }
        req.tweets = tweets;
        next();
    });
};
