const Relations = require("../models/relations");
//const { errorHandler } = require("../helpers/dbErrorHandler");

// middlewares rest

exports.userRelationById = (req, res, next, id) => {
    Relations.findById(id).exec((err, relation) => {
        if (err || !relation) {
            return res.status(404).json({
                error: "Relation does not exist"
            });
        }
        req.relation = relation;
        next();
    });
};
