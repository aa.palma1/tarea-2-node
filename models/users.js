const mongoose = require ("mongoose");
const { truncate } = require("lodash");

const userSchema = new mongoose.Schema(
    {
        names:{
            type: String,
            trim: true,
            required: true
        },
        surnames:{
            type: String,
            trim: true,
            required: true
        },
        password: {
            type: String,
            required: true,
            trim: true
         },
        birthday:{
            type: Date,
            trim: true,
            default: Date.now()
        },
        email:{
            type: String,
            trim: true,
            required: true,
            unique: true,
        },
        avatar:{
            type: String,
            trim: true,
            default: ''
        },
        banner:{
            type: String,
            trim: true,
            default: ''
        },
        biography:{
            type: String,
            trim: true,
            default: ''
        },
        location:{
            type: String,
            trim: true,
            default: ''
        },
        website:{
            type: String,
            trim: true,
            default: ''
        },
    },
    {timestamps: true}
);




module.exports = mongoose.model("Users",userSchema);
 